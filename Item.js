e strict';
/**
 * Abstract item service
 * Represents single object with properties, provides a way to load and save the data
 * The object can be instantiated as follows
 * <code>
 *     new Item({
 *          id: 'appprofile-0',
 *          objectName: 'applicationprofile',
 *          data: {...}
 *          bind: {
 *              ...
 *          }
 *     });
 * </code>
 *
 * Event Listeners:
 * onBeforeLoad: Is called before preloading the object
 * itemLoadSuccess: Is called when the object preload was succesfull (response is passed as a parameter)
 * itemLoadFail: Is called when object preload failed (response is passed as a parameter)
 * itemBeforeCreate: Is called before going to create state
 * itemBeforeEdit: Is called before going to edit state
 * itemBeforeSave: Is called before sending save request
 * itemSaveSuccess: Is called when save was succesfull (response is passed as a parameter)
 * itemSave: Is called after save was completed or failed (response is passed as a parameter)
 * itemCreate: Called when item was created
 * itemUpdate: Callend when item was updated
 * itemSaveFail: Is called when save failed (response is passed as a parameter)
 * itemChange: Is called when item.set() was executed to update the data (updates are passed as a parameter)
 * itemBeforeDrop: Is called before delete request sent
 * itemDrop: Is called after delete was completed or failed (response is passed as a parameter)
 * itemDropSuccess: Is called when delete was succesfull (response is passed as a parameter)
 * itemDropFail: Is called after delete fail (response is passed as a parameter)
 */
aviApp.factory('Item', ['$q', 'Base', 'AviModal', 'Auth', 'DeepDiff',
    function ($q, Base, AviModal, Auth, DeepDiff) {

    /**
     * Just a constructor
     * @param oArgs {object} - Configuration object, contains argument list, event listeners and more
     * @constructor
     */
    var Item = function(oArgs) {
        // Call superclass constructor
        Item.superconstructor.call(this, oArgs);
        // Initialize
        this.objectName = oArgs && oArgs.objectName ? oArgs.objectName : this.objectName;
        this.itemClass = oArgs && oArgs.itemClass ? oArgs.itemClass : this.itemClass;
        this.params = oArgs && oArgs.params ? oArgs.params : angular.copy(this.params);
        this.id = oArgs && oArgs.id ? oArgs.id : this.id;
        this.windowElement = oArgs && oArgs.windowElement ? oArgs.windowElement : this.windowElement;
        this.collection = oArgs && oArgs.collection ? oArgs.collection : this.collection;
        this.data = oArgs && oArgs.data ? oArgs.data : this.data;
        if(this.data && !this.id && oArgs) {
            this.id = this.getIdFromData(oArgs.data);
        }
    };

    /**
     * Inherit from Base
     */
    avi.inherit(Item, Base);

    /**
     * Keeps lowercased object name like 'applicationprofile'
     * Used to build the url for making calls
     * @type {string}
     */
    Item.prototype.objectName = null;

    /**
     * Keeps id of the current object (the slug)
     * @type {string}
     */
    Item.prototype.id = null;

    /**
     * Actual data of the object
     * @type {object}
     */
    Item.prototype.data = null;

    /**
     * DOM element of the window that creates/edits the current object
     * @type {DOMelement}
     */
    Item.prototype.windowElement = null;

    /**
     * Url parameters, append to url
     * @type {{include_name: boolean}}
     */
    Item.prototype.params = {
        include_name: true
    };

    /**
     * Loading indicator
     * @type {boolean}
     */
    Item.prototype.busy = false;

    /**
     * The promise that is used to catch save operation
     * @type {promise}
     */
    Item.prototype.modalPromise = null;

    /**
     * Generating and returning the params needed to be sent in order to load the collection data
     * @returns {Array} Array of parameters
     */
    Item.prototype.getLoadParams = function() {
        return _.map(this.params, function(val, key) {
            return key + '=' + val
        });
    };

    /**
     * Sending load request
     * Gives an option to have multiple requests wrapped by $q
     * @returns {promise}
     */
    Item.prototype.loadRequest = function() {
        var params = this.getLoadParams();
        return this.request('get', '/api/' + this.objectName + '/' + this.id + (params.length ? '?' + params.join('&') : ''));
    };

    /**
     * In case if backend returned an error it's going to be here
     * @type {object}
     */
    Item.prototype.errors = null;

    /**
     * Loads the object data from server
     * @returns {promise}
     */
    Item.prototype.load = function(fields) {
        var self = this;
        var deferred = $q.defer();
        if(this.id) {
            this.trigger('onBeforeLoad');
            this.busy = true;
            this.loadRequest(fields).then(function(rsp) {
                self.data = rsp.data || {};
                self.data = self.transformAfterLoad(self.data);
                self.busy = false;
                self.trigger('itemLoad itemLoadSuccess', rsp);
                deferred.resolve(rsp);
            }, function(errors) {
                self.errors = errors.data;
                if(!errors.silent) {
                    AviModal.alert(self.errors.error || self.errors.__all__ || 'Could not load object of type ' + self.objectName + ' with id ' + self.id);
                }
                self.busy = false;
                self.trigger('itemLoad itemLoadFail', errors);
                deferred.reject(errors);
            });
        } else {
            deferred.reject({
                error: 'Object ID is missing'
            });
        }
        return deferred.promise;
    };

    Item.prototype.transformAfterLoad = function(data) {
        return data;
    };

    /**
     * This function should return the object that will be sent to server upon save
     * If the object requires cleaning before save then this is a place to do that
     * @returns {Object} - Should return configuration data that is ready for sending
     */
    Item.prototype.dataToSave = function() {
        return this.data;
    };

    Item.prototype.urlToSave = function() {
        return '/api/' + this.objectName + (this.id ? '/' + this.id : '') + '?include_name';
    };

    Item.prototype.transformDataAfterSave = function(rsp) {
        // If response contains array of object, the last object is the one that we need
        return rsp.data instanceof Array ? rsp.data[rsp.data.length-1] : rsp.data;
    };

    /**
     * Sending save request
     * If you need to make multiple save calls you may override this function
     * and use $q.all() to wrap calls and return the promise
     * @returns {promise}
     */
    Item.prototype.saveRequest = function() {
        return this.request(this.id ? 'put' : 'post', this.urlToSave(), this.dataToSave());
    };

    /**
     * Saves the object. Sends request to the server using objectName
     * @returns {promise}
     */
    Item.prototype.save = function() {
        var self = this;
        var deferred = $q.defer();
        if(this.busy) {
            deferred.reject();
            return deferred.promise;
        }
        this.trigger('itemBeforeSave');
        this.busy = true;
        this.saveRequest().then(function(rsp) {
            self.data = self.transformDataAfterSave(rsp);
            self.set(self.data);
            self.busy = false;
            var action = self.id ? 'update' : 'create';
            if(self.collection) {
                // Update collection with new data (happens immedialtely)
                if(self.id) {
                    // Find it in the collection and put the data
                    self.collection.get(self.id).set(self.data);
                    if(self.opener) {
                        self.opener.set(self.data);
                    }
                } else {
                    // Just append as new item
                    self.collection.append(self);
                }
                // Reload collection to be sure the data is in sync with the server
                // it should be seamless to the user
                self.collection.load();
            }
            if(!self.id) {
                self.id = self.getIdFromData(self.data);
            }
            self.trigger('itemSave itemSaveSuccess', self);
            if('create' == action) {
                self.trigger('itemCreate', self);
            } else {
                self.trigger('itemUpdate', self);
            }
            deferred.resolve(rsp);
        }, function(errors) {
            self.errors = errors.data;
            self.busy = false;
            self.trigger('itemSave itemSaveFail', errors);
            deferred.reject(errors);
        });
        return deferred.promise;
    };

    /**
     * Sets the data without replacing the object
     * @param dataToUpdate {object} - Property list to update
     */
    Item.prototype.set = function(dataToUpdate) {
        if(this.data==undefined) {
            this.data = {};
        }
        angular.extend(this.data, dataToUpdate);
        this.trigger('itemChange', dataToUpdate);
        if(this.collection) {
            this.collection.trigger('collectionChange', dataToUpdate);
        }
        return this;
    };

    Item.prototype.getIdFromData = function(data) {
        return data && data.url ? data.url.slug() : null;
    };

    /**
     * Deletes the item. Sends request to server
     * @param force
     * @returns {Promise.promise|*}
     */
    Item.prototype.drop = function(force) {
        var deferred = $q.defer();
        var self = this;
        if(this.isDroppable()) {
            this.trigger('itemBeforeDrop');
            this.request('delete', '/api/' + this.objectName + '/' + this.id + (force ? '?force_delete' : '')).then(function (rsp) {
                if (self.collection) {
                    var data = self.collection.data;
                    for (var ii = 0; ii < data.length; ii++) {
                        if (self.getIdFromData(data[ii]) == self.id) {
                            data.splice(ii, 1);
                            self.collection.total--;
                            break;
                        }
                    }
                }
                self.trigger('itemDrop itemDropSuccess', rsp);
                deferred.resolve(rsp);
            }, function (errors) {
                if (!errors.silent) {
                    AviModal.alert(errors.data.error || errors.data.__all__ || 'Error occured while deleting an object');
                }
                self.trigger('itemDrop itemDropFail', errors);
                deferred.reject(errors);
            });
        } else {
            deferred.reject({error: 'Can not delete system default object'});
        }
        return deferred.promise;
    };

    /**
     * Gives an option to prepare data or do anything else before going to edit mode
     */
    Item.prototype.beforeEdit = function() {
        // redeclare this function on your manner
    };

    /**
     * Opens edit window using AviModal
     */
    Item.prototype.edit = function (windowElement, params) {
        // Editable object must have cloned data to avoid mess
        var editable = this.collection.get({
            id: this.id,
            data:angular.copy(this.data),
            windowElement: windowElement || this.windowElement
        });
        editable.opener = this;
        // Share events
        editable.events = this.events;
        editable.modalPromise = $q.defer();
        if(!editable.data) {
            editable.load().then(function() {
                editable.beforeEdit();
                editable.trigger('itemBeforeEdit');
            });
        } else {
            editable.beforeEdit();
            editable.trigger('itemBeforeEdit');
        }
        // Set the object to be unchanged initially
        editable.setPristine();
        params = params ? params : {};
        params.editable = editable;
        AviModal.open(editable.windowElement, params);
        return editable.modalPromise.promise;
    };

    /**
     * Returns true if the data has been modified
     * @return {boolean}
     */
    Item.prototype.modified = function() {
        var diff = DeepDiff.diff(this.data, this.backup);
        if(!diff) {
            return false;
        }
        // Go deeper into diff and make sure that this is real change, not the $$hashKey
        return _.any(diff, function(change) {
            return ({
                do: function(change) {
                    if(change.item) {
                        return this.do(change.item);
                    } else {
                        return change.path && change.path[change.path.length-1]!='$$hashKey';
                    }
                }
            }).do(change);
        });
    };

    /**
     * Marks the object as pristine, i.e. unchanged
     */
    Item.prototype.setPristine = function() {
        this.backup = angular.copy(this.data);
    };

    /**
     * Closes create/edit mode, raises confirm dialog if object has changes
     */
    Item.prototype.dismiss = function(silent) {
        if(!silent && this.modified() && !confirm('Dismiss changes?')) {
            return;
        }
        $(this.windowElement).aviModal('hide');
    };

    /**
     * Saves current object and then hides window
     * @returns {promise}
     */
    Item.prototype.submit = function () {
        var self = this;
        var deferred = $q.defer();
        // Save it and close the window
        this.save().then(function(rsp) {
            if(self.windowElement) {
                $(self.windowElement).aviModal('hide');
            }
            deferred.resolve(rsp);
            if(self.modalPromise) {
                self.modalPromise.resolve(self);
            }
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    };

    /**
     * Return true if edit function is available for this object
     * @returns {boolean}
     */
    Item.prototype.isEditable = function() {
        // If edit screen is not available for this object
        // Then obviously it can't be edited
        if(!this.windowElement) {
            return false;
        }

        // Admin context can edit anything in case of write permission
        if(Auth.context.tenant_ref.slug()=='admin' && Auth.isAllowed(this.objectName, 'w')) {
            return true;
        }
        // If the tenant is owner of this object than it's editable
//        if(this.objectName!='user' && this.data && this.data.tenant_ref && Auth.context.tenant_ref.slug()==this.data.tenant_ref.slug()) {
//            return true;
//        }
        if(this.data && this.data.tenant_ref && Auth.context.tenant_ref.slug()==this.data.tenant_ref.slug()
            && Auth.isAllowed(this.objectName, 'w')) {
            return true;
        }
//        if(this.isProtected() && Auth.context.tenant_ref.slug()!='admin') {
//            return false;
//        }
//        if(Auth.isAllowed(this.objectName, 'w')) {
//            return true;
//        }
        return false;
    };

    /**
     * Return true if delete action is allowed for this object
     * @returns {boolean}
     */

    Item.prototype.isDroppable = function() {
        // Protected items can't be deleted
        if(this.isProtected()) {
            return false;
        }
        // Admin context - allowed
        if(Auth.context.tenant_ref.slug()=='admin' && Auth.isAllowed(this.objectName, 'w')) {
            return true;
        }
        // If the tenant is owner then allowed
        if(this.data && this.data.tenant_ref && Auth.context.tenant_ref.slug()==this.data.tenant_ref.slug()
            && Auth.isAllowed(this.objectName, 'w')) {
            return true;
        }
        return false;
    };

    /**
     * Returns true if this object is protected (cannot be deleted)
     * @return {boolean}
     */
    Item.prototype.isProtected = function() {
        var self = this;
        var protectedItems = Auth.defaults.default[this.objectName.toLowerCase()];
        if(protectedItems) {
            return !!_.find(protectedItems, function(item) {
                return item == self.id;
            });
        }
        return false;
    };

    return Item;


}]);
