/**
 * Abstract grid
 * Helps to display table data with ability to select multiple rows and do something with them
 * It doesn't support pagination since it doesn't communicate with server, it just works with local array data
 * The directive requires defining configuration object that looks as follows
 *
 *  <code>
 *  $scope.serversGrid = {
 *       // Contains the list of fields
 *       fields: [{
 *           name:'enabled',
 *           title:'Status',
 *           // Transform function helps to make correction to the value before showing it in the view
 *           transform: function(row) {
 *               return row.enabled ? 'Enabled' : 'Disabled';
 *           },
 *           sortBy: 'enabled'
 *       },{
 *           name:'hostname',
 *           title:'Host Name',
 *           // Template gives ability to customize cell layout
 *           // 2 way data binding works as well so you may even do like <input ng-model="row.field">
 *           // You can use row and field vars inside the template
 *           template: '<span>{{row.hostname}}</span>'
 *       },{
 *           name:'ip',
 *           title:'IP Address',
 *           transform: function(row) {
 *               return row.ip.addr;
 *           },
 *           sortBy: function(a, b){
 *             if (a.ip.addr > b.ip.addr) return 1;
 *             else if (a.ip.addr < b.ip.addr) return -1;
 *             else return 0;
 *           }
 *       }],
 *       // This function is very important since it determines unique row id
 *       // It can be either string containing the name of the unique field like ID
 *       // or a function that returns a string representing the row that is passed to it
 *       rowId: function(row) {
 *           return [
 *               row.ip.addr,
 *               row.port,
 *               row.ratio
 *           ].join('-');
 *       },
 *       // Multiple actions is a list of action functions and a title
 *       // `do` function is a function that perform action on selected items
 *       // `do` function should return true to clear selection after the action
 *       multipleactions: [{
 *           title: 'Remove',
 *           do: function(servers) {
 *               PoolConfig.data.servers = _.filter(PoolConfig.data.servers, function(row) {
 *                   return !_.find(servers, function(server) {
 *                       if(row==server) {
 *                           delete($scope.serversCache[server.hostname + '-' + server.ip.addr + '-' + server.nw_ref.slug()]);
 *                           return true;
 *                       }
 *                   });
 *               });
 *               return true;
 *           }
 *       }],
 *       // Single action is just to display row buttons on the right, like delete button
 *       // don't forget to determine class (usually fontawesome or bootstrap class)
 *       // `do` function is doing all the stuff for you with the row passed to it
 *       singleactions: [{
 *           title: 'Delete',
 *           class: 'icon-trash',
 *           do: function(row) {
 *               PoolConfig.data.servers = _.filter(PoolConfig.data.servers, function(item) {
 *                   return item!=server;
 *               });
 *           }
 *       }],
 *       // Row class is just giving ability to put your custom class into the row
 *       rowClass: function(row) {
 *           return !row.enabled ? 'disabled' : '';
 *       }
 *   };
 *   </code>
 *
 */

aviApp.directive('grid', ['$filter', '$window', '$compile', '$timeout', '$document',
    function ($filter, $window, $compile, $timeout, $document) {
    return {
        scope: {
            rows: '=',
            config: '=',
            ngDisabled: '=',
        },
        restrict: 'E',
        templateUrl: 'src/views/components/grid.html',
        link: function (scope, elm, attr) {
            scope.attr = attr;

            /**
             * Keeps selection state
             * @type object - keeps key>value pairs where key is a unique id of the row (see rowId for details)
             */
            scope.config.selected = {};
            scope.config.allSelected = false;
            // keeping sorted order with new data
            if (scope.config.currentlySortedBy) scope.config.currentlySortedBy();

            /**
             * Selects/Deselects all rows in the grid
             */
            scope.toggleAll = function(e) {
                scope.config.selected = {};
                if(scope.config.allSelected) {
                    _.each($filter('filter')(scope.rows, scope.config.search), function(row) {
                        if(!scope.config.checkboxDisable(row)) {
                            scope.config.selected[scope.rowId(row)] = true;
                        }
                    });
                }
            };

            scope.select = function(s){
                if (s === 'all') {
                    scope.config.allSelected = true;
                    scope.toggleAll();
                } else if (s === 'clear'){
                    scope.config.allSelected = false;
                    scope.config.selected = {};
                }
            };

            scope.show = function(s){
                if (s === 'selected') {
                    scope.config.search = function(a){
                        if (typeof a === 'object') return (scope.config.selected[scope.rowId(a)]);
                    };
                } else if (s === 'all'){
                    scope.config.search = '';
                }
            };

            /**
             * Resets grid state
             * Clears selections and search filter
             */
            scope.config.reset = function() {
                scope.config.selected = {};
                scope.config.allSelected = false;
                scope.config.search = '';
            };

            scope.detailsPanel = null;
            if(!scope.config.onRowClick && scope.config.expandedContainerTemplate) {
                scope.config.onRowClick = function(row, event) {
                    scope.row = row;
                    var tr = $(event.target).closest('tr');
                    if(!tr.hasClass('expanded')) {
                        scope.closePanels();
                        scope.detailsPanel = $($compile(
                            '<tr class="details">' +
                                '<td colspan="' +
                                    (scope.config.fields.length + (scope.config.multipleactions ? 1 : 0) + (scope.config.singleactions ? 1 : 0)) + '">' +
                                    scope.config.expandedContainerTemplate +
                                '</td>' +
                            '</tr>'
                        )(scope));
                        scope.detailsPanel.insertAfter(tr);
                        tr.addClass('expanded');
                    } else {
                        scope.closePanels();
                    }
                };
            }

            scope.closePanels = function() {
                $(elm).find('tr').removeClass('expanded');
                angular.element(scope.detailsPanel).remove();
                scope.detailsPanel = null;
            };

            var elContainer;

            /**
             * Shows options panel
             */
            scope.showOptions = function(event) {
                if(scope.ngDisabled) {
                    return;
                }
                elContainer = $(event.target);
                elContainer.addClass('active');
                $(elm).find('.selection-panel').show();
                $(elm).find('.drop-mask').show();
                scope.expanded = true;
                var updatePanelSizeAndPosition = function() {
                    var offset = elContainer.offset();
                    offset.top += elContainer.height() - $($window).scrollTop();
                    $(elm).find('.selection-panel').css(offset);
                };
                updatePanelSizeAndPosition();
                $($window).resize(updatePanelSizeAndPosition);
            };

            scope.selectAll = function(){
                scope.config.allSelected = true;
                scope.toggleAll();
            };

            scope.clearAll = function() {
                scope.config.allSelected = false;
                scope.config.selected = {};
            };

            scope.showSelected = false;

            scope.selectionFilter = function(row) {
                if(!scope.showSelected) {
                    return true;
                }
                return scope.config.selected[scope.rowId(row)];
            };

            scope.showSelection = function() {
                scope.showSelected = true;
            };

            scope.hideSelection = function() {
                scope.showSelected = false;
            };

            /**
             * Hides options panel
             */
            scope.hideOptions = function() {
                $(elm).find('.selection-panel').hide();
                $(elm).find('.drop-mask').hide();
                elContainer.removeClass('active');
                scope.expanded = false;
                $($window).unbind('resize scroll');
            };

            /**
             * Cleans selected object (removing false items)
             */
            scope.toggleItem = function(a) {
                for(var item in scope.config.selected) {
                    if(!scope.config.selected[item]) {
                        delete(scope.config.selected[item]);
                    }
                }
                var filtered = $filter('filter')(scope.rows, scope.config.search);
                // Filter out disabled checkboxes
                if(typeof scope.config.checkboxDisable == 'function') {
                    filtered = _.filter(filtered, function(row) {
                        return !scope.config.checkboxDisable(row)
                    });
                }
                if(filtered.length) {
                    scope.config.allSelected = !_.find(filtered, function(row) {
                        return !scope.config.selected[scope.rowId(row)];
                    });
                } else {
                    scope.config.allSelected = false;
                }
            };

            // Helper function to find attribute based on string
            // e.g. if you have "health_score.average_scores.health_score" if will return
            // obj.health_score.average_scores.health_score
            var findAttr = function (obj, fieldName) {
                var a = fieldName.split('.');
                if(obj && a && a[0]) {
                    return findAttr(obj[a.splice(0,1)], a.join('.'));
                }
                return obj;
            };

            // Will either sort table by the attribute defined by the given string (see findAttr)
            // or use the given function to sort the table
            scope.sort = function(field){
                scope.closePanels();
                scope.order = scope.currentlySortedByObj == field ? !scope.order : false;
                if (typeof field.sortBy === 'string'){
                    scope.rows.sort(function(obj1, obj2){
                        var a = findAttr(obj1, field.sortBy);
                        var b = findAttr(obj2, field.sortBy);
                        if (typeof b === 'number'){
                            if (scope.order) return b - a;
                            else return a - b;
                        } else {
                            if (b > a) return scope.order ? 1 : -1;
                            else if (b < a) return scope.order ? -1 : 1;
                            else return 0;
                        }
                    });
                } else if (typeof field.sortBy === 'function'){
                    scope.rows.sort(field.sortBy);
                    if (!scope.order) scope.rows.reverse();
                } else {
                    console.warn("sort requires either a function or string to sort by");
                }
                //so that we can easily resort the table when we get new data
                scope.currentlySortedByObj = field;
                scope.currentlySortedBy = function(){
                    scope.sort(field);
                };
            };

            /**
             * Returns true if at least one row is selected
             * @returns {boolean}
             */
            scope.someSelected = function() {
                return !!Object.keys(scope.config.selected).length;
            };

            /**
             * Returns id of the row. Using config.rowId to determine unique string for the row
             * @param row {object} - Row object
             * @returns {string} - row id or null if config.rowId is not defined which is not normal
             */
            scope.rowId = function(row) {
                if(typeof scope.config.rowId == 'function') {
                    return scope.config.rowId(row);
                } else if(typeof scope.config.rowId == 'string') {
                    return row[scope.config.rowId];
                }
            };

            /**
             * Returns selected rows
             * @returns {Array}
             */
            scope.getSelected = function() {
                return _.filter(scope.rows, function(row) {
                    if(scope.config.selected[scope.rowId(row)]) {
                        return true;
                    }
                });
            };

            scope.multipleActionDisabled = function(action) {
                if(!scope.someSelected() || scope.ngDisabled) {
                    return true;
                }
                if(typeof action.disabled=='function') {
                    return action.disabled.call(scope, scope.getSelected());
                } else if(action.disabled) {
                    return !!action.disabled;
                }
                return false;
            };
            scope.singleActionDisabled = function(row, action) {
                if(scope.ngDisabled) {
                    return true;
                }
                if(typeof action.disabled=='function') {
                    return action.disabled.call(scope, row);
                } else if(action.disabled) {
                    return !!action.disabled;
                }
                return false;
            };

            /**
             * Calls action on selected rows, after that clears selection if the action returned true
             * @param action {object} - The action object (defined in config)
             * @param event {event} - Event object, just in case we need it in the action
             */
            scope.doMultipleAction = function(action, event) {
                scope.closePanels();
                if(action.do.call(scope, scope.getSelected(), event, this)) {
                    scope.config.selected = {};
                }
            };

            /**
             * Calls single action (row button clicked)
             * @param row {object} - Row object
             * @param action {object} - The action object (defined in config)
             */
            scope.doSingleAction = function(row, action, index) {
                scope.closePanels();
                if(scope.singleActionDisabled(row, action, index)) {
                    return;
                }
                action.do.call(scope, row, index);
            };

            // This code looks odd, but it works. scope !== scope because of angular magic
            // It's so that our little mini menu goes away when you click anywhere else
            var $doc = $(document);
            var clickSelectOptionsScope;
            $doc.off('click.main');
            $doc.on('click.main', function(){
                var cs = clickSelectOptionsScope;
                if (cs){
                    cs.selectOptions = false;
                    cs.$digest();
                }
            });

            // Getting rid of listeners
            scope.$on('$destroy', function(){
                $doc.off('click.main');
            });


            scope.clickSelectOptions = function(e){
                e.stopPropagation();
                clickSelectOptionsScope = scope;
                scope.selectOptions = !scope.selectOptions;
            };

            /**
             * Returns the array of column percentage sizes
             * @return {array}
             */
            var getColPercentSizes = function() {
                var columns = $(elm).find('thead > tr:first-child > th');
                // Get the total width of the table
                var total = 0;
                _.each(columns, function(col) {
                    total += $(col).width();
                });
                return _.map(columns, function(col) {
                    // Get the size of each column
                    return ((100/total)*$(col).width()).toPrecision(4);
                });
            };

            /**
             * Column resize logic
             */
            $timeout(function() {
                // Watch for mouse position
                var mouseX, mouseStartX, trigger, recalcFields;
                $($document).mousemove(function(event) {
                    mouseX = event.pageX;
                    if(trigger) {
                        var th = $(trigger).parent().parent();
                        var dist = mouseX - mouseStartX;
                        mouseStartX = mouseX;
                        // Track minimum and maximum width of the column
                        // Don't allow reduce columns less than 40px
                        var thWidth = th.width();
                        if(thWidth + dist < 40) {
                            dist += (40 - (thWidth + dist));
                        }
                        thWidth = th.next().width();
                        if(thWidth - dist < 40) {
                            dist -= (40 - (thWidth - dist));
                        }

                        th.width(th.width() + dist);
                        th.next().width(th.next().width() - dist);
                    }
                });
                $($window).on('resize.collection-grid', function(e) {
                    if(recalcFields) {
                        var columns = $(elm).find('thead > tr > th');
                        var total = 0;
                        _.each(columns, function(col) {
                            total += $(col).width();
                        });
                        _.each(columns, function(col) {
                            $(col).css({width: ((100/total)*$(col).width()) + '%'});
                        });
                        recalcFields = false;
                    }
                });
                $(elm).find('.resize').click(function(event) {
                    event.stopPropagation();
                }).mousedown(function(event) {
                    if(!$(event.currentTarget).parent().parent().next()) {
                        // If next column is not available
                        return;
                    }
                    trigger = event.currentTarget;
                    mouseStartX = mouseX;

                    var columns = $(elm).find('thead > tr > th');
                    var total = 0;
                    _.each(columns, function(col) {
                        total += $(col).width();
                    });
                    _.each(columns, function(col) {
                        $(col).width($(col).width());
                    });
                    recalcFields = true;
                }).dblclick(function(event) {
                    var col = $(event.currentTarget).parent().parent();
                    // Find column index
                    var colIndex;
                    _.find(col.parent().children(), function(sibling, index) {
                        colIndex = index;
                        return sibling == col[0];
                    });
                    var maxWidth = 0, width;
                    _.each($(elm).find('tbody').children(), function(row) {
                        width = $($(row).children()[colIndex]).find('cell').width();
                        if(width > maxWidth) {
                            maxWidth = width;
                        }
                    });
                    var th = col;
                    var dist = maxWidth - col.width() + 20;
                    th.width(th.width() + dist);
                    th.next().width(th.next().width() - dist);
                });
                $(elm).mouseup(function() {
                    trigger = null;
                    // Restore percentage sizes for columns
                    var sizes = getColPercentSizes();
                    sizes.splice(0,1);
                    sizes.splice(-1,1);
                    var columns = $(elm).find('thead > tr:first-child > th');
                    columns.splice(0,1);
                    columns.splice(-1,1);
                    _.each(columns, function(col, index) {
                        $(col).css('width', sizes[index] + '%');
                    });
                });
            });

        }
    }
}]);

/**
 * Cell directive
 * Helps to transform data for display
 * Works with field.transform function to transform data
 * and with field.template property as a template
 */
aviApp.directive('cell', ['$compile', 'Auth',  function ($compile, Auth) {
    return {
        scope: {
            row: '=',
            field: '=',
            config: '=',
        },
        restrict: 'E',
        link: function (scope, elm, attr) {
            scope.Auth = Auth;
            if(!scope.field) {
                return;
            }
            if(typeof scope.field.transform == 'function') {
                elm.append($compile('<span>{{field.transform(row)}}</span>')(scope));
            } else if(scope.field.template) {
                elm.append($compile(scope.field.template)(scope));
            } else if(scope.field.name) {
                elm.append($compile('<span>{{row.' + scope.field.name + '}}</span>')(scope));
            }
        }
    }
}]);
