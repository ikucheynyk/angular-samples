'use strict';

/**
 * Abstract collection service
 * Provides a way to load/edit/delete list data
 * The collection can be instantiated as follows
 * <code>
 *     new Collection({
 *          objectName: 'applicationprofile',
 *          bind: {
 *              ...
 *          }
 *     });
 * </code>
 *
 * Event Listeners:
 * collectionBeforeLoad: Is called before data load
 * collectionLoadSuccess: Is called when load was successful (response is passed as a parameter)
 * collectionLoad: Is called after load was successful or failed (response or error is passed as a parameter)
 * collectionLoadFail: Is called only after load was failed (error is passed as a parameter)
 */
aviApp.factory('Collection', ['$q', 'Base', 'Item', 'AviModal', 'Auth',
    function ($q, Base, Item, AviModal, Auth) {

    /**
     * Just a constructor
     * @param oArgs {object} - Configuration object, contains argument list, event listeners and more
     * @constructor
     */
    var Collection = function(oArgs) {
        // Call superclass constructor
        Collection.superconstructor.call(this, oArgs);
        // Initialize
        this.params = oArgs && oArgs.params ? oArgs.params : angular.copy(this.params);
        this.objectName = oArgs && oArgs.objectName ? oArgs.objectName : this.objectName;
        this.itemClass = oArgs && oArgs.itemClass ? oArgs.itemClass : this.itemClass;
        this.data = oArgs && oArgs.data ? oArgs.data : this.data;
        this.sortBy = oArgs && oArgs.sortBy ? oArgs.sortBy : this.sortBy;
        this.windowElement = oArgs && oArgs.windowElement ? oArgs.windowElement : this.windowElement;
        this.defaults = oArgs && oArgs.defaults ? oArgs.defaults : angular.copy(this.defaults);
        this.schema = oArgs && oArgs.schema ? oArgs.schema : this.schema;
    };

    /**
     * Inherit from Base
     */
    avi.inherit(Collection, Base);

    /**
     * Keeps lowercased object name like 'applicationprofile'
     * Used to build the url for making calls
     * @type {string}
     */
    Collection.prototype.objectName = '';

    /**
     * Keeps the class that is used to create instances of nested objects
     * @type {*}
     */
    Collection.prototype.itemClass = Item;

    /**
     * Just real array, this is data storage for the object
     * @type {Array}
     */
    Collection.prototype.data = null;

    /**
     * DOM element of the window that creates/edits the current object
     * @type {DOMelement}
     */
    Collection.prototype.windowElement = null;


    /**
     * Defaults. When creating new object these values will be predefined
     * @type {object}
     */
    Collection.prototype.defaults = null;

    /**
     * Sorts the collection by this field
     * @type {string}
     */
    Collection.prototype.sortBy = null;

    /**
     * Holds loading page, on every loadNext() this property will be incremented
     * @type {integer}
     */
    Collection.prototype.page = 1;

    /**
     * Keeps information about next page availability on the server
     * If this property is false, grid will stop loading next pages
     * @type {boolean}
     */
    Collection.prototype.nextPageAvailable = null;

    /**
     * Holds total amount of items including the ones that were not loaded yet
     * @type {number}
     */
    Collection.prototype.total = 0;

    Collection.prototype.params = {
        include_name: true
    };

    /**
     * Loading indicator
     * @type {boolean}
     */
    Collection.prototype.busy = false;

    /**
     * In case if backend returned an error, it's going to be here
     * @type {null}
     */
    Collection.prototype.errors = null;

    /**
     * Generating and returning the params needed to be sent in order to load the collection data
     * @returns {Array} Array of parameters
     */
    Collection.prototype.getLoadParams = function(page) {
        var params = _.map(this.params, function(val, key) {return key + '=' + val});
        if(this.sortBy) {
            params.push('sort=' + this.sortBy);
        }
        if(this.filter) {
            params.push('search=' + this.filter);
        }
        if(page > 1) {
            params.push('page=' + page);
        }
        return params;
    };

    /**
     * Sending load request
     * If you need to make multiple calls you may override this function
     * and use $q.all() to wrap calls and return the promise
     * @returns {promise}
     */
    Collection.prototype.loadRequest = function(page) {
        var params = this.getLoadParams(page);
        var url = '/api/' + this.objectName + (params.length ? '?' + params.join('&') : '');
        return this.request('get', url);
    };

    /**
     * Loads the data from server using objectName
     * @param page {integer} - Unsgned number of page (starting from 1)
     * @param append {boolean} - Indicates whether to append or override the data
     * @returns {promise}
     */
    Collection.prototype.load = function(page, append) {
        var self = this;
        if(!page) {
            page = 1
        }
        var deferred = $q.defer();
        this.trigger('collectionBeforeLoad');
        if(!this.objectName) {
            deferred.reject({
                error: 'Unable to load collection data, `objectName` missing'
            });
            console.log('Unable to load collection data, `objectName` missing');
            return deferred.promise;
        }
        this.busy = true;
        this.loadRequest(page).then(function(rsp) {
            self.page = page;
            rsp.data = self.transformAfterLoad(rsp.data);
            if(rsp.data.results) {
                if(append) {
                    // Append
                    self.data = self.data.concat(rsp.data.results);
                } else {
                    // Override
                    self.data = rsp.data.results;
                }
            }
            self.busy = false;
            if(self.nextPageAvailable==null || self.nextPageAvailable == true) {
                self.nextPageAvailable = !!rsp.data.next;
            }
            self.total = rsp.data.count;
            self.trigger('collectionLoad collectionLoadSuccess', rsp);
            deferred.resolve(rsp);
        }, function(errors) {
            self.errors = errors.data;
            if(!errors.silent) {
                AviModal.alert(self.errors.error || self.errors.__all__ || 'Could not load objects of type ' + self.objectName);
            }
            self.busy = false;
            self.trigger('collectionLoad collectionLoadFail', errors);
            deferred.reject(errors);
        });
        return deferred.promise;
    };

    Collection.prototype.transformAfterLoad = function(data) {
        return data;
    };

    Collection.prototype.loadNext = function() {
        if(!this.nextPageAvailable || this.busy) {
            return;
        }
        this.load(this.page+1, true);
    };

    Collection.prototype.reset = function() {
        this.data = [];
        this.page = null;
        this.nextPageAvailable = null;
        this.filter = null;
    };

    Collection.prototype.sort = function(sSortBy) {
        if(this.sortBy==sSortBy) {
            this.sortBy = '-' + sSortBy;
        } else {
            this.sortBy = sSortBy;
        }
        this.load();
    };

    Collection.prototype.search = function(sQuery) {
        this.filter = sQuery;
        this.page = null;
        this.nextPageAvailable = null;
        return this.load();
    };

    /**
     * Creates an instance of item and returns it
     * There are several ways to call this function:
     *    1. collection.get(<object-id>) - Using string object id
     *    2. collection.get({data:[...]}) - Using configuration object
     * Configuration object is the following
     * {
     *   id: Object slug (i prefer to name it ID)
     *   data: Data that will be placed in the object. Is optional.
     *   bind: A set of event listeners for the item
     * }
     *
     * @param opts {object} - Configuration object
     * @returns {item} Returns an instance of itemClass
     */
    Collection.prototype.get = function(opts) {
        // If opts is string - assume it is ID
        if(typeof opts == 'string') {
            var opts = {id: opts};
        }
        // If object has id and doesn't have data
        // find the data in the array
        if(opts.id && !opts.data) {
            opts.data = _.find(this.data, function(item) {
                return item.url.slug() == opts.id;
            });
        }
        // Object name has to be the same
        opts.objectName = opts.objectName || this.objectName;
        opts.windowElement = opts.windowElement || this.windowElement;
        opts.collection = this;
        // Create instance
        var newItem = new this.itemClass(opts);
        // Share the events
        newItem.events = this.events;
        // Create instance of Item and return
        return newItem;
    };

    /**
     * Creates a new instance of itemClass with all data set to defaults
     * @returns {item}
     */
    Collection.prototype.newItem = function() {
        // Get schema defaults
        var newItem = this.getDefault();
        // Create instance of Item with defaults
        return this.get({
            data: newItem
        });
    };

    /**
     * Returns default object
     */
    Collection.prototype.getDefault = function() {
        // Get default object
        var def = Auth.defaults[this.objectName.toLowerCase()];
        def = angular.copy(def || {});
        // Then override it with custom defaults
        if(typeof this.defaults == 'function') {
            this.defaults(def);
        } else {
            for(var key in this.defaults) {
                def[key] = angular.copy(this.defaults[key]);
            }
        }
        return def;
    };

    /**
     * Appends the object into the collection
     * @param item
     */
    Collection.prototype.append = function(item) {
        if(!(this.data instanceof Array)) {
            this.data = [];
        }
        this.data.push(item.data);
    };

    /**
     * Opens create dialog with AviModal
     * @param winElement {domElement} - Dom element (or query string) of the window element
     */
    Collection.prototype.create = function (windowElement, params) {
        var editable = this.newItem();
        if(windowElement) {
            editable.windowElement = windowElement;
        }
        return editable.edit(undefined, params);
    };

    /**
     * Return true if create function is available for this object
     * @returns {boolean}
     */
    Collection.prototype.isCreatable = function() {
        return !!this.windowElement && Auth.isAllowed(this.objectName, 'w');
    };

    return Collection;
}]);

