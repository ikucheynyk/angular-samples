/**
 * Dropdown form element
 * Custom select form element (like select2)
 *
 * Supported Attributes:
 * ng-model, ng-disabled, placeholder, multiple, ng-change
 *
 * Example of usage:
 * <dropdown ng-model="asd" multiple="true" placeholder="Select option">
 *      <option value="4">Test 4</option>
 *      <option value="5">Test 5</option>
 *      <option value="6">Test 6</option>
 * </dropdown>
 */
aviApp.directive('dropdown', ['$timeout', '$window', function ($timeout, $window) {
    return {
        scope: {
            ngModel: '=',
            ngChange: '&',
            onclear: '&',
            ngDisabled: '=',
            ngRequired: '=',
            allowClear: '@',
            search: '@'
        },
        restrict: 'E',
        templateUrl: 'src/views/components/dropdown.html',
        require:'ngModel',
        transclude: true,
        link: function (scope, elm, attr, ctrl) {
            // Need this in template
            scope.attr = attr;

            // Keeps panel expanded state. Used in hotkeys
            scope.expanded = false;

            /**
             * Shortcuts to DOM elements
             * @type {jQuery}
             */
            var elContainer = $(elm).children('.dropdown-container');
            var elOptions = $(elm).children('.options');
            var elOverlay = $(elm).children('.drop-mask');
            var elScrollable = $(elm).children('.scrollable');

            /**
             * Hotkeys
             */
            elContainer.unbind('keydown').keydown(function(event) {
                if(event.keyCode==40) {
                    if(!scope.expanded) {
                        scope.showOptions();
                    } else {
                        scope.select(scope.getNextItem());
                    }
                } else if(event.keyCode==38) {
                    if(scope.expanded) {
                        scope.select(scope.getPrevItem());
                    }
                } else if(event.keyCode==27) {
                    scope.hideOptions();
                    event.preventDefault();
                    event.stopPropagation();
                } else if(event.keyCode==13) {
                    scope.hideOptions();
                }
            });

            /**
             * Watching and validating ngModel
             */
            scope.$watch('ngModel', function() {
                scope.validate();
            });

            scope.validate = function() {
                if(scope.ngRequired) {
                    ctrl.$setValidity('required', scope.isNotEmpty());
                }
            };

            /**
             * Hides options panel when overlay clicked
             */
            elOverlay.mousedown(function() {
                scope.hideOptions();
            });

            // Do it initially, otherwise selected item may not be shown properly
            $timeout(function() {
                scope.validate();
            });

            /**
             * Returns true if something selected
             * @returns {boolean}
             */
            scope.isNotEmpty = function() {
                var res;
                if(attr.multiple != undefined) {
                    res = scope.ngModel !== undefined && scope.ngModel !== null && scope.ngModel.length > 0;
                } else {
                    res = scope.ngModel !== '' && scope.ngModel !== undefined && scope.ngModel !== null;
                }
                return res;
            };

            /**
             * Finds the option object by value
             * @param value
             * @returns {object} - option
             */
            scope.findOptionByValue = function(value) {
                var optEl = _.find($(elm).find('.data-options').children(), function(optEl) {
                    return (optEl.value || $(optEl).text()) == value;
                });
                if(optEl) {
                    return {
                        value: optEl.value || $(optEl).text(),
                        label: $(optEl).text()
                    };
                }
                return null;
            };

            /**
             * Finds the option by value and returns it's label
             * Used to display selected option label
             * @param value - option value
             * @returns {string}
             */
            scope.getLabel = function(value) {
                var option = scope.findOptionByValue(value);
                if(option) {
                    return option.label || option.name() || option;
                }
//                if(value) {
//                    return value.name() || value;
//                }
                return '';
            };

            /**
             * Shows options panel
             */
            scope.showOptions = function() {
                if(scope.ngDisabled) {
                    return;
                }
                // Read DOM to get option list
                scope.options = _.map($(elm).find('.data-options').children(), function(optEl) {
                    return {
                        value: optEl.value || $(optEl).text(),
                        label: $(optEl).text(),
                        tooltip: $(optEl).attr('tooltip')
                    };
                });
                elOptions.show();
                elOverlay.show();
                scope.expanded = true;
                scope.query = '';
                elScrollable.scrollTop(0);
                var updatePanelSizeAndPosition = function() {
//                    var offset = elContainer.offset();
//                    offset.top += elContainer.height() - $($window).scrollTop();
//                    elOptions.css(offset);
                    elOptions.width(elContainer.width());
                };
                updatePanelSizeAndPosition();
                $($window).resize(updatePanelSizeAndPosition);
//                $($window).scroll(updatePanelSizeAndPosition);
            };

            /**
             * Hides options panel
             */
            scope.hideOptions = function() {
                elOptions.hide();
                elOverlay.hide();
                scope.expanded = false;
                $($window).unbind('resize');
            };

            /**
             * Selects the item
             * @param item {object}
             */
            scope.select = function(item) {
                if(attr.multiple!=undefined) {
                    if(!(scope.ngModel instanceof Array)) {
                        scope.ngModel = [];
                    }
                    // make sure there will be no duplicates
                    if(!_.find(scope.ngModel, function(selected) {return selected==item || selected==item.value})) {
                        scope.ngModel.push(item.value || item);
                    }
                } else {
                    scope.ngModel = item.value || item;
                }
                scope.validate();
                $timeout(function() {
                    // Call ngChange
                    if(attr.multiple!=undefined) {
                        var selected = _.map(scope.ngModel, scope.findOptionByValue);
                    } else {
                        var selected = scope.findOptionByValue(scope.ngModel);
                    }
                    scope.ngChange({
                        selected: selected
                    });
                });
            };

            /**
             * Returns the next item after selected
             * @returns {object}
             */
            scope.getNextItem = function() {
                var index;
                for(var ii=0; ii<scope.options.length; ii++) {
                    if(scope.ngModel == scope.options[ii] || scope.ngModel == scope.options[ii].value) {
                        index = ii;
                        break;
                    }
                }
                if(scope.options[index+1]) {
                    return scope.options[index+1];
                }
            };

            /**
             * Returns previous item after selected
             * @returns {object}
             */
            scope.getPrevItem = function() {
                var index;
                for(var ii=0; ii<scope.options.length; ii++) {
                    if(scope.ngModel.slug() == scope.options[ii].url.slug()) {
                        index = ii;
                        break;
                    }
                }
                if(scope.options[index-1]) {
                    return scope.options[index-1];
                }
            };

            /**
             * Removes one of the selected items (multiple select only)
             * @param index {index} - Element's index
             */
            scope.remove = function(index) {
                scope.ngModel.splice(index, 1);
                scope.validate();
            };

            /**
             * Filters options to hide items that are already selected
             * Works only in multiple mode
             * @param item {object} - Option item
             * @returns {boolean} - true if visible
             */
            scope.filterHideSelected = function(item) {
                if(scope.ngModel instanceof Array) {
                    return !_.find(scope.ngModel, function(selected) {
                        return selected==item || selected == item.value;
                    });
                }
                return true;
            };

            /**
             * Clears field value
             */
            scope.clear = function() {
                scope.ngModel = undefined;
                scope.onclear();
            };

        }
    }
}]);

