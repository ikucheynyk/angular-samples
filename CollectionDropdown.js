/**
 * Dropdown form element
 * Comparing to regular <select> and select2 element it gives an option to create new and modify existing option
 * It works with abstract Collection, which handles all server communication (if needed), creation, modification and search.
 *
 * Example of usage:
 * <collection-dropdown ng-model="action.value.pool_ref" options="PoolCollection" placeholder="Select Pool"></collection-dropdown>
 *
 */
aviApp.directive('collectionDropdown', ['$timeout', '$window', function ($timeout, $window) {
    return {
        scope: {
            ngModel: '=',
            options: '=',
            ngChange: '&',
            onclear: '&',
            ngDisabled: '=',
            ngRequired: '=',
            allowClear: '@',
            search: '@'
        },
        restrict: 'E',
        templateUrl: 'src/views/components/collection-dropdown.html',
        require:'ngModel',
        link: function (scope, elm, attr, ctrl) {
            scope.attr = attr;

            /**
             * Shortcuts to DOM elements
             * @type {jQuery}
             */
            var elContainer = $(elm).find('.dropdown-container');
            var elOptions = $(elm).find('.options');
            var elOverlay = $(elm).find('.drop-mask');
            var elScrollable = $(elm).find('.scrollable');
            scope.expanded = false;

            /**
             * Hotkeys
             */
            elContainer.unbind('keydown').keydown(function(event) {
                if(event.keyCode==40) {
                    if(!scope.expanded) {
                        scope.showOptions();
                    } else {
                        scope.select(scope.getNextItem());
                    }
                } else if(event.keyCode==38) {
                    if(scope.expanded) {
                        scope.select(scope.getPrevItem());
                    }
                } else if(event.keyCode==69) {
                    scope.edit();
                } else if(event.keyCode==67) {
                    scope.options.create();
                } else if(event.keyCode==27) {
                    scope.hideOptions();
                    event.preventDefault();
                    event.stopPropagation();
                } else if(event.keyCode==13) {
                    scope.hideOptions();
                }
            });

            /**
             * Watching and validating ngModel
             */
            scope.$watch('ngModel', function() {
                scope.validate();
                if(scope.selectedItem && scope.selectedItem.data && scope.ngModel
                    && scope.ngModel.slug()==scope.selectedItem.data.url.slug()) {
                    // Nothing changed
                    return;
                }
                if(scope.ngModel && scope.ngModel.slug) {
                    if(!scope.options.data) {
                        scope.options.load().then(function() {
                            scope.selectedItem = scope.options.get(scope.ngModel.slug());
                        });
                    } else {
                        scope.selectedItem = scope.options.get(scope.ngModel.slug());
                    }
                } else {
                    scope.selectedItem = null;
                }
            });

            /**
             * Infinite scroll logic
             * (Debounce and throttle didn't work well, did it on my own)
             */
            var timer;
            elScrollable.scroll(function() {
                var dist2bottom = elScrollable[0].scrollHeight - (elScrollable.scrollTop() + elScrollable.height());
                if(dist2bottom < 200) {
                    $timeout.cancel(timer);
                    timer = $timeout(function() {
                        // Load next page
                        scope.options.loadNext();
                    }, 50);
                }
            });

            /**
             * Hides options panel when overlay clicked
             */
            elOverlay.mousedown(function() {
                scope.hideOptions();
            });

            /**
             * Asks collection to open create modal
             */
            scope.create = function() {
                scope.options.create().then(function(item) {
                    // When new item created or saved need to refresh selection
                    scope.select(item.data);
                });
                scope.hideOptions();
            };

            /**
             * Returns true if something selected
             * @returns {boolean}
             */
            scope.isNotEmpty = function() {
                return scope.ngModel instanceof Array ? scope.ngModel.length>0 : scope.ngModel;
            };

            $timeout(function() {
                scope.validate();
            });

            /**
             * Tries to get the name of the option
             * @param value - option value
             * @returns {string}
             */
            scope.getLabel = function(option) {
                if(option) {
                    if(typeof option.name == 'function') {
                        return option.name();
                    } else if(typeof option.name == 'string') {
                        return option.name;
                    } else if(option.slug()) {
                        return option.slug();
                    } else {
                        return option;
                    }
                }
                return '';
            };

            /**
             * Shows options panel
             */
            scope.showOptions = function() {
                if(scope.ngDisabled) {
                    return;
                }
                elOptions.show();
                elOverlay.show();
                scope.expanded = true;
                scope.filter = '';
                scope.options.reset();
                scope.options.load();
                elScrollable.scrollTop(0);
                var updatePanelSizeAndPosition = function() {
//                    var offset = elContainer.offset();
//                    offset.top += elContainer.height() - $($window).scrollTop();
//                    elOptions.css(offset);
                    elOptions.width(elContainer.width());
                };
                updatePanelSizeAndPosition();
                $($window).resize(updatePanelSizeAndPosition);
                $($window).scroll(updatePanelSizeAndPosition);
            };

            /**
             * Hides options panel
             */
            scope.hideOptions = function() {
                elOptions.hide();
                elOverlay.hide();
                scope.expanded = false;
                $($window).unbind('resize scroll');
            };

            /**
             * Selects the item
             * @param item {object}
             */
            scope.select = function(item) {
                if(attr.multiple!=undefined) {
                    if(!(scope.ngModel instanceof Array)) {
                        scope.ngModel = [];
                    }
                    // make sure there will be no duplicates
                    if(!_.find(scope.ngModel, function(url) {return url==item.url})) {
                        scope.ngModel.push(item.url);
                    }
                } else {
                    scope.ngModel = item.url;
                    scope.selectedItem = scope.options.get(scope.ngModel.slug());
                }
                $timeout(function() {
                    // Call ngChange
                    scope.ngChange({
                        selected: scope.options.get({data:item})
                    });
                });
                scope.validate();
            };

            scope.validate = function() {
                if(scope.ngRequired) {
                    ctrl.$setValidity('required', scope.isNotEmpty());
                }
                if(attr.multiple) {
                    var len = scope.ngModel instanceof Array ? scope.ngModel.length : 0;
                    if(attr.min) {
                        ctrl.$setValidity('min', len >= parseInt(attr.min));
                    }
                    if(attr.max) {
                        ctrl.$setValidity('max', len <= parseInt(attr.max));
                    }
                }
            };

            /**
             * Returns the next item after selected
             * @returns {object}
             */
            scope.getNextItem = function() {
                var index;
                for(var ii=0; ii<scope.options.data.length; ii++) {
                    if(scope.ngModel.slug() == scope.options.data[ii].url.slug()) {
                        index = ii;
                        break;
                    }
                }
                if(scope.options.data[index+1]) {
                    return scope.options.data[index+1];
                }
            };

            /**
             * Returns previous item after selected
             * @returns {object}
             */
            scope.getPrevItem = function() {
                var index;
                for(var ii=0; ii<scope.options.data.length; ii++) {
                    if(scope.ngModel.slug() == scope.options.data[ii].url.slug()) {
                        index = ii;
                        break;
                    }
                }
                if(scope.options.data[index-1]) {
                    return scope.options.data[index-1];
                }
            };

            /**
             * Removes one of the selected items (multiple select only)
             * @param index {index} - Element's index
             */
            scope.remove = function(index) {
                scope.ngModel.splice(index, 1);
                scope.validate();
            };

            /**
             * Filters options to hide items that are already selected
             * Works only in multiple mode
             * @param item {object} - Option item
             * @returns {boolean} - true if visible
             */
            scope.filterHideSelected = function(item) {
                if(scope.ngModel instanceof Array) {
                    return !_.find(scope.ngModel, function(selected) {
                        return selected.slug() == item.url.slug();
                    });
                }
                return true;
            };

            scope.optionSelected = function(item) {
                if(!scope.ngModel || typeof(scope.ngModel)!='string') {
                    return false;
                }
                return item.url.slug()==scope.ngModel.slug();
            };

            /**
             * Filters collection, debouncing multiple calls
             * Used to search in options
             * (Debounce and throttle didn't work well, did it on my own)
             */
            var searchTimer = null;
            scope.doSearch = function(query) {
                $timeout.cancel(searchTimer);
                searchTimer = $timeout(function() {
                    scope.options.search(query);
                }, 500);
            };

            scope.isEditable = function() {
                if(attr.multiple) {
                    return false;
                }
                return scope.selectedItem && scope.selectedItem.isEditable && scope.selectedItem.isEditable();
            };

            /**
             * Triggers collection to open edit dialog
             */
            scope.edit = function() {
                if(scope.ngDisabled) {
                    return;
                }
                var editable = scope.options.get(scope.ngModel.slug());
                if(editable.isEditable()) {
                    editable.load().then(function() {
                        editable.edit().then(function() {
                            // Manually update selected item without triggering ng-change
                            scope.ngModel = editable.data.url;
                            scope.selectedItem = scope.options.get(scope.ngModel.slug());
                        });
                    });
                }
            };

            /**
             * Clears field value
             */
            scope.clear = function() {
                scope.ngModel = undefined;
                scope.onclear();
            };
        }
    }
}]);
