e strict';

/**
 * Abstract service
 * Provides basic functions for services to communicate with server and more
 */
aviApp.factory('Base', ['$q', '$http', function ($q, $http) {

    /**
     * Just constructor
     * @param oArgs {object} - Argument list
     * @constructor
     */
    var Base = function(oArgs) {
        this.events = {};
        this.outstandingRequestCancel = [];
        if(oArgs) {
            this.bindMultiple(oArgs.bind);
        }
    };

    /**
     * Define eventlisteners
     * @type {object}
     */
    Base.prototype.events = {};

    /**
     * Just makes a call to the url provided
     * @param method {string} - HTTP method
     * @param url {string} - Url to sed request to
     * @param data {object} - Object to send (optional)
     * @returns {promise}
     */
    Base.prototype.request = function(method, url, data) {
        var deferred = $q.defer(),
            cancelRequestDefered = $q.defer(),
            self = this;
        // Remove hash (for IE)
        url = url.replace(/#.+$/, '');
        $http({
            method: method,
            url: url,
            data: data,
            timeout: cancelRequestDefered.promise
        }).then(function(rsp) {
            deferred.resolve(rsp);
        }, function(rsp) {
            deferred.reject(rsp);
        }).finally(function() {
            self.outstandingRequestCancel
                .splice(self.outstandingRequestCancel.indexOf(cancelRequestDefered), 1);
        });

        self.outstandingRequestCancel.push(cancelRequestDefered);

        return deferred.promise;
    };


    /**
     * Cancels all pending requests using promise
     */
    Base.prototype.cancelRequests = function() {
        var i;
        for(i =0; i < this.outstandingRequestCancel.length; i++){
            this.outstandingRequestCancel[i].resolve();
        }
    };

    /**
     * Adds multiple event listeners.
     * @param oListeners {object}
     */
    Base.prototype.bindMultiple = function(oListeners) {
        var iListeners, vListener, iListener;
        for (var sEvent in oListeners) {
            if (oListeners.hasOwnProperty(sEvent)) {
                vListener = oListeners[sEvent];
                if (vListener instanceof Array) {
                    iListeners = vListener.length;
                    for (iListener = 0; iListener < iListeners; iListener++) {
                        this.bind(sEvent, vListener[iListener]);
                    }
                } else {
                    this.bind(sEvent, vListener);
                }
            }
        }
    };

    /**
     * Creates event listener
     * @param sEvent {string} - The name of event
     * @param fListener {function} - Callback function
     * @param bOnetime {boolean} - If true then the event will be removed after event fire
     */
    Base.prototype.bind = function(sEvent, fListener, bOnetime, bUnshift) {
        if (typeof fListener != 'function') {
            return false;
        }
        // If multiple events
        var aEvents = sEvent.split(' ');
        if(aEvents.length > 1) {
            for(var iEvent=0; iEvent<aEvents.length; iEvent++) {
                this.bind.apply(this, [aEvents[iEvent]].concat([].slice.call(arguments, 1)));
            }
            return;
        }
        var oEvents = this.events;
        var oEvent = oEvents[sEvent];
        if (!oEvent) {
            oEvents[sEvent] = {
                listeners: []
            };
            oEvent = oEvents[sEvent];
        } else {
            this.unbind(sEvent, fListener);
        }
        if (bOnetime) {
            oEvent.listeners[bUnshift ? 'unshift' : 'push']({
                listener: fListener,
                onetime: true
            });
        } else {
            oEvent.listeners[bUnshift ? 'unshift' : 'push'](fListener);
        }
    };

    /**
     * Shortcut to bind
     * @param sEvent {string} - The name of event
     * @param fListener {function} - Callback function
     * @param bOnetime {boolean} - If true then the event will be removed after event fire
     */
    Base.prototype.on = function(sEvent, fListener, bOnetime) {
        this.bind(sEvent, fListener, bOnetime);
    };

    /**
     * Removes event listener
     * @param sEvent {string} - Event name
     * @param fListener {function} - Callback function
     * @returns {number} - Number of removed listeners
     */
    Base.prototype.unbind = function(sEvent, fListener) {
        // If multiple events
        var aEvents = sEvent.split(' ');
        if(aEvents.length > 1) {
            for(var iEvent=0; iEvent<aEvents.length; iEvent++) {
                this.bind.apply(this, [aEvents[iEvent]].concat([].slice.call(arguments, 1)));
            }
            return;
        }
        var oEvents = this.events;
        if (!oEvents[sEvent]) {
            return 0;
        }
        var aListeners = oEvents[sEvent].listeners;
        var iRemoved = 0;
        var oListener;
        for (var iListener = aListeners.length - 1; iListener >= 0; iListener--) {
            oListener = aListeners[iListener];
            if (oListener == fListener || oListener.listener == fListener) {
                aListeners.splice(iListener, 1);
                iRemoved++;
            }
        }
        return iRemoved;
    };

    /**
     * Removes ontime event listeners
     * @param sEvent {string} - Event name
     * @returns {number} - Number of removed listeners
     */
    Base.prototype.removeOnetimeEventListeners = function(sEvent) {
        // If multiple events
        var aEvents = sEvent.split(' ');
        if(aEvents.length > 1) {
            for(var iEvent=0; iEvent<aEvents.length; iEvent++) {
                this.removeOnetimeEventListeners.apply(this, [aEvents[iEvent]].concat([].slice.call(arguments, 1)));
            }
            return;
        }
        var oEvents = this.events;
        if (!oEvents[sEvent]) {
            return 0;
        }
        var aListeners = oEvents[sEvent].listeners;
        var iRemoved = 0;
        for (var iListener = aListeners.length - 1; iListener >= 0; iListener--) {
            if (aListeners[iListener].onetime) {
                aListeners.splice(iListener, 1);
                iRemoved++;
            }
        }
        return iRemoved;
    };

    /**
     * Fires event. Multiple arguments allowed, will be passed to listener functions
     * @param sEvent {string} - Event name
     */
    Base.prototype.trigger = function(sEvent) {
        // If multiple events
        var aEvents = sEvent.split(' ');
        if(aEvents.length > 1) {
            for(var iEvent=0; iEvent<aEvents.length; iEvent++) {
                this.trigger.apply(this, [aEvents[iEvent]].concat([].slice.call(arguments, 1)));
            }
            return;
        }
        var oEvents = this.events;
        if (!oEvents[sEvent]) {
            return;
        }
        // Duplicate array because it may be modified from within the listeners
        var aListeners = oEvents[sEvent].listeners.slice();
        var iListeners = aListeners.length;
        var aArgs, oListener;
        for (var iListener = 0; iListener < iListeners; iListener++) {
            // Remove first argument
            aArgs = [].slice.call(arguments, 1);
            // Call event listener in scope of this object
            oListener = aListeners[iListener];
            if (typeof oListener == 'function') {
                oListener.apply(this, aArgs);
            } else {
                oListener.listener.apply(this, aArgs);
            }
        }
        // Remove one-time event listeners
        this.removeOnetimeEventListeners(sEvent);
    };

    return Base;

}]);

