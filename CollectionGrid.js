viApp.directive('collectionGrid', ['$filter', '$timeout', '$window', '$document', 'Auth', '$compile',
    function ($filter, $timeout, $window, $document, Auth, $compile) {
    return {
        scope: {
            config: '=',
            ngDisabled: '=',
        },
        restrict: 'E',
        templateUrl: 'src/views/components/collection-grid.html',
        link: function (scope, elm, attr) {
            scope.attr = attr;

            /**
             * Keeps selection state
             * @type object - keeps key>value pairs where key is a unique id of the row (see rowId for details)
             */
            scope.config.selected = {};

            /**
             * Select all checkbox model
             * @type {boolean}
             */
            scope.config.allSelected = false;

            /**
             * Selects/Deselects all rows in the grid
             */
            scope.toggleAll = function(e) {
                scope.config.selected = {};
                if(scope.config.allSelected) {
                    _.each(scope.config.collection.data, function(row) {
                        if(!scope.config.checkboxDisable(row)) {
                            scope.config.selected[scope.rowId(row)] = true;
                        }
                    });
                }
            };

            /**
             * Resets grid state
             * Clears selections and search filter
             */
            scope.config.reset = function() {
                scope.config.selected = {};
                scope.config.allSelected = false;
                scope.config.search = '';
                scope.config.collection.search('');
            };

            scope.detailsPanel = null;
            if(!scope.config.onRowClick && scope.config.expandedContainerTemplate) {
                scope.config.onRowClick = function(row, event) {
                    scope.row = row;
                    var tr = $(event.target).closest('tr');
                    if(!tr.hasClass('expanded')) {
                        scope.closePanels();
                        scope.detailsPanel = $($compile(
                                '<tr class="details">' +
                                '<td colspan="' +
                                (scope.config.fields.length + (scope.config.multipleactions ? 1 : 0) + (scope.config.singleactions ? 1 : 0)) + '">' +
                                scope.config.expandedContainerTemplate +
                                '</td>' +
                                '</tr>'
                        )(scope));
                        scope.detailsPanel.insertAfter(tr);
                        tr.addClass('expanded');
                    } else {
                        scope.closePanels();
                    }
                };
            }
            scope.closePanels = function() {
                $(elm).find('tr').removeClass('expanded');
                angular.element(scope.detailsPanel).remove();
                scope.detailsPanel = null;
            };

            var elContainer;

            /**
             * Shows options panel
             */
            scope.showOptions = function(event) {
                if(scope.ngDisabled) {
                    return;
                }
                elContainer = $(event.target);
                var elOptions = $(elContainer).parent().find('.selection-panel');
                var elOverlay = $(elContainer).parent().find('.drop-mask');
                elContainer.addClass('active');
                elOptions.show();
                elOverlay.show();
                scope.expanded = true;
                var updatePanelSizeAndPosition = function() {
                    var offset = elContainer.offset();
                    offset.top += elContainer.height() - $($window).scrollTop();
                    elOptions.css(offset);
                };
                updatePanelSizeAndPosition();
                $($window).resize(updatePanelSizeAndPosition);
            };

            scope.selectAll = function(){
                scope.config.allSelected = true;
                scope.toggleAll();
            };

            scope.clearAll = function() {
                scope.config.allSelected = false;
                scope.config.selected = {};
            };

            scope.showSelected = false;

            scope.selectionFilter = function(row) {
                if(!scope.showSelected) {
                    return true;
                }
                return scope.config.selected[scope.rowId(row)];
            };

            /**
             * Hides options panel
             */
            scope.hideOptions = function() {
                $(elm).find('.selection-panel').hide();
                $(elm).find('.drop-mask').hide();
//                elOverlay.hide();
                elContainer.removeClass('active');
                scope.expanded = false;
                // $($window).unbind('resize scroll');
                $($window).off('resize.collection-grid');
                $($window).off('scroll.collection-grid');

            };

            /**
             * Cleans selected object (removing false items)
             */
            scope.toggleItem = function(a) {
                for(var item in scope.config.selected) {
                    if(!scope.config.selected[item]) {
                        delete(scope.config.selected[item]);
                    }
                }
                var filtered = $filter('filter')(scope.config.collection.data, scope.config.search);
                // Filter out disabled checkboxes
                filtered = _.filter(filtered, function(row) {
                    return !scope.config.checkboxDisable(row)
                });
                if(filtered.length) {
                    scope.config.allSelected = !_.find(filtered, function(row) {
                        return !scope.config.selected[scope.rowId(row)];
                    });
                } else {
                    scope.config.allSelected = false;
                }
            };

            /**
             * Returns true if at least one row is selected
             * @returns {boolean}
             */
            scope.someSelected = function() {
                return !!Object.keys(scope.config.selected).length;
            };

            /**
             * Returns id of the row. Using config.rowId to determine unique string for the row
             * @param row {object} - Row object
             * @returns {string} - row id or null if config.rowId is not defined which is not normal
             */
            scope.rowId = function(row) {
                if(typeof scope.config.rowId == 'function') {
                    return scope.config.rowId(row);
                } else if(typeof scope.config.rowId == 'string') {
                    return row[scope.config.rowId];
                }
            };

            /**
             * Returns selected rows
             * @returns {Array}
             */
            scope.getSelected = function() {
                return _.filter(scope.config.collection.data, function(row) {
                    if(scope.config.selected[scope.rowId(row)]) {
                        return true;
                    }
                });
            };

            scope.multipleActionDisabled = function(action) {
                if(!scope.someSelected() || scope.ngDisabled) {
                    return true;
                }
                if(typeof action.disabled=='function') {
                    return action.disabled.call(scope, scope.getSelected());
                } else if(action.disabled) {
                    return !!action.disabled;
                }
                return false;
            };
            scope.singleActionDisabled = function(row, action) {
                if(scope.ngDisabled) {
                    return true;
                }
                if(typeof action.disabled=='function') {
                    return action.disabled.call(scope, row);
                } else if(action.disabled) {
                    return !!action.disabled;
                }
                return false;
            };

            /**
             * Calls action on selected rows, after that clears selection if the action returned true
             * @param action {object} - The action object (defined in config)
             * @param event {event} - Event object, just in case we need it in the action
             */
            scope.doMultipleAction = function(action, event) {
                scope.closePanels();
                if(action.do.call(scope, scope.getSelected(), event, this)) {
                    scope.config.selected = {};
                }
            };

            /**
             * Calls single action (row button clicked)
             * @param row {object} - Row object
             * @param action {object} - The action object (defined in config)
             */
            scope.doSingleAction = function(row, action) {
                scope.closePanels();
                if(scope.singleActionDisabled(row, action)) {
                    return;
                }
                action.do.call(scope, row);
            };

            /**
             * Filters collection, debouncing multiple calls
             */
            var searchTimer = null;
            scope.search = function() {
                $timeout.cancel(searchTimer);
                searchTimer = $timeout(function() {
                    scope.config.collection.search(scope.config.search);
                }, 500);
            };

            scope.sort = function(field) {
                if(field.name) {
                    scope.config.collection.sort(field.sortBy);
                    scope.closePanels();
                }
            };

            scope.load = function() {
                // Initial sorting
                if(scope.config.defaultSorting) {
                    scope.config.collection.sort(scope.config.defaultSorting);
                } else {
                    scope.config.collection.load();
                }
                scope.closePanels();
            };
            scope.load();

            /**
             * This is just to hide/show fixed header, that makes sticky effect
             */
            scope.updateFixedHeaderVisibility = function() {
                var thead2 = $(elm[0]).find('thead > tr.floating');
                var panel = $(elm).find('.actions-panel');
                var panelFloating = $(elm).find('.actions-panel.floating');
                if(scope.scrollable[0] != $window) {
                    var dist = scope.scrollable.offset().top - $(panel[0]).offset().top;
                    if(dist && dist > 0) {
                        thead2.show();
                        panelFloating.show();
                    } else {
                        thead2.hide();
                        panelFloating.hide();
                    }
                } else {
                    if($('body').scrollTop() > 37) {
                        thead2.show();
                        panelFloating.show();
                    } else {
                        thead2.hide();
                        panelFloating.hide();
                    }
                }
            };

            /**
             * Makes attempt to load next page, debouncing multiple calls
             */
            var timer;
            scope.loadNextIfSpaceAvailable = function() {
                var dist2bottom = $($document).height() - $($window).scrollTop() - $($window).height();
                // If the scrollbar is less than 500px down to the bottom
                if(dist2bottom < 200) {
                    if(timer) {
                        $timeout.cancel(timer);
                    }
                    timer = $timeout(function() {
                        // Load next page
                        scope.config.collection.loadNext();
                    }, 300);
                }
            };

            /**
             * Restores sticky header sizes to be consistent to its real header
             * Some explanation: Template has 2 headers, the first one is real and the other one is sticky with position:static (hidden by default)
             * Since static position detaches the element from the table it loosing it's sizes
             * this function manually restores sizing
             */
            var restoreStickyHeaderSizing = function() {
                var thead1 = $(elm[0]).find('thead > tr:first-child')[0];
                var thead2 = $(elm[0]).find('thead > tr.floating')[0];
                $(thead2).width($(thead1).width());
                var ths1 = $(thead1).children();
                var ths2 = $(thead2).children();
                _.each(ths1, function(th1, index) {
                    $(ths2[index]).width($(th1).width());
                });
                $(thead2).width($(thead1).width()+1);
                $(elm).find('.actions-panel.floating').width($(thead1).width()+2);
            };
            scope.getClassFromName = function(name) {
                return name ? name.replace(/\./g, '-') : '';
            };

            /**
             * Returns the array of column percentage sizes
             * @return {array}
             */
            var getColPercentSizes = function() {
                var columns = $(elm).find('thead > tr:first-child > th');
                // Get the total width of the table
                var total = 0;
                _.each(columns, function(col) {
                    total += $(col).width();
                });
                return _.map(columns, function(col) {
                    // Get the size of each column
                    return ((100/total)*$(col).width()).toPrecision(4);
                });
            };

            /**
             * Saves column sizes into current user's ui_property
             */
            var saveFieldsState = function() {
                // Disable this feature temporary
                return;
                if(!Auth.myAccount.data.ui_property.grid_field_size) {
                    Auth.myAccount.data.ui_property.grid_field_size = {};
                }
                if(!Auth.myAccount.data.ui_property.grid_field_size[scope.config.collection.objectName]) {
                    Auth.myAccount.data.ui_property.grid_field_size[scope.config.collection.objectName] = {};
                }
                var sizes = getColPercentSizes();
                // Remove firsta and last items because they relate to non resizable columns
                sizes.splice(0,1);
                sizes.splice(-1,1);
                Auth.myAccount.data.ui_property.grid_field_size[scope.config.collection.objectName] = sizes;
                Auth.myAccount.saveDelayed();
            };

            /**
             * Recursively find nearest scrollable element
             * @param el - starting element
             * @returns {element} - DOM element
             */
            var findScrollable = function(el) {
                if(!el || !el[0]) {
                    return null;
                } else if ($.inArray($(el).css('overflowY'), ['scroll', 'auto']) != -1
                    || $.inArray($(el).css('overflow'), ['scroll', 'auto']) != -1) {
                    // Found scrollable container
                    return el;
                }
                return findScrollable(el.parent());
            };

            /**
             * Find the nearest scrollable element
             * need that to position fixed header at the top of scrollable element
             */
            scope.scrollable = findScrollable(elm);
            if(scope.scrollable[0].tagName.toLowerCase()!='body') {
                var panel = $(elm).find('.actions-panel.floating');
                panel.css('top', scope.scrollable.offset().top);
                var th = $(elm).find('thead > tr.floating');
                th.css('top', scope.scrollable.offset().top + panel.height());
            } else {
                scope.scrollable = $($window);
            }

            /**
             * Column resize logic
             */
            $timeout(function() {
                // Restore column sizes
                // Feature disabled temporary
/*
                if(Auth.myAccount.data.ui_property.grid_field_size
                    && Auth.myAccount.data.ui_property.grid_field_size[scope.config.collection.objectName]) {
                    var sizes = Auth.myAccount.data.ui_property.grid_field_size[scope.config.collection.objectName];
                    var columns = $(elm).find('thead > tr:first-child > th');
                    // Remove first and last items because they relate to non resizable columns
                    columns.splice(0,1);
                    columns.splice(-1,1);
                    if(sizes.length==columns.length) {
                        _.each(columns, function(col, index) {
                            $(col).css('width', sizes[index] + '%');
                        });
                    }
                }
*/
                // Watch for mouse position
                var mouseX, mouseStartX, trigger, recalcFields;
                var headers = $(elm).find('thead tr');
                $($document).mousemove(function(event) {
                    mouseX = event.pageX;
                    if(trigger) {
                        var th = $(trigger).parent().parent().parent();
                        var dist = mouseX - mouseStartX;
                        mouseStartX = mouseX;
                        // Track minimum and maximum width of the column
                        // Don't allow reduce columns less than 40px
                        var thWidth = th.width();
                        if(thWidth + dist < 40) {
                            dist += (40 - (thWidth + dist));
                        }
                        thWidth = th.next().width();
                        if(thWidth - dist < 40) {
                            dist -= (40 - (thWidth - dist));
                        }

                        th.width(th.width() + dist);
                        th.next().width(th.next().width() - dist);
                        if(th.parent()[0]==headers[1]) {
                            var thead2 = $(elm[0]).find('thead > tr:first-child')[0];
                            var thead1 = $(elm[0]).find('thead > tr.floating')[0];
                            $(thead2).width($(thead1).width());
                            var ths1 = $(thead1).children();
                            var ths2 = $(thead2).children();
                            _.each(ths1, function(th1, index) {
                                $(ths2[index]).width($(th1).width());
                            });
                        }
                    }
                });
                $win.on('resize.collection-grid', function() {
                    if(recalcFields) {
                        var columns = $(elm).find('thead > tr:first-child > th');
                        var total = 0;
                        _.each(columns, function(col) {
                            total += $(col).width();
                        });
                        _.each(columns, function(col) {
                            $(col).css({width: ((100/total)*$(col).width()) + '%'});
                        });
                        recalcFields = false;
                    }
                });
                $(elm).find('.resize').click(function(event) {
                    event.stopPropagation();
                }).mousedown(function(event) {
                    if(!$(event.currentTarget).parent().parent().parent().next()) {
                        // If next column is not available
                        return;
                    }
                    // Remember mouse start position and element that triggered resize
                    trigger = event.currentTarget;
                    mouseStartX = mouseX;
                    // Make sure all columns have pixes sizes, otherwise it's gonna mess everything
                    var columns = $(elm).find('thead > tr:first-child > th');
                    var total = 0;
                    _.each(columns, function(col) {
                        total += $(col).width();
                    });
                    _.each(columns, function(col) {
                        $(col).width($(col).width());
                    });
                    recalcFields = true;
                }).dblclick(function(event) {
                    var col = $(event.currentTarget).parent().parent().parent();
                    // Find column index
                    var colIndex;
                    _.find(col.parent().children(), function(sibling, index) {
                        colIndex = index;
                        return sibling == col[0];
                    });
                    var maxWidth = 0, width;
                    _.each($(elm).find('tbody').children(), function(row) {
                        width = $($(row).children()[colIndex]).find('cell').width();
                        if(width > maxWidth) {
                            maxWidth = width;
                        }
                    });
                    var th = col;
                    var dist = maxWidth - col.width() + 20;
                    th.width(th.width() + dist);
                    th.next().width(th.next().width() - dist);
                    if(th.parent()[0]==headers[1]) {
                        var thead2 = $(elm[0]).find('thead > tr:first-child')[0];
                        var thead1 = $(elm[0]).find('thead > tr.floating')[0];
                        $(thead2).width($(thead1).width());
                        var ths1 = $(thead1).children();
                        var ths2 = $(thead2).children();
                        _.each(ths1, function(th1, index) {
                            $(ths2[index]).width($(th1).width());
                        });
                    }
                });
                $(elm).mouseup(function() {
                    trigger = null;
                    restoreStickyHeaderSizing();
                    saveFieldsState();
                    // Restore percentage sizes for columns
                    var sizes = getColPercentSizes();
                    sizes.splice(0,1);
                    sizes.splice(-1,1);
                    var columns = $(elm).find('thead > tr:first-child > th');
                    columns.splice(0,1);
                    columns.splice(-1,1);
                    _.each(columns, function(col, index) {
                        $(col).css('width', sizes[index] + '%');
                    });
                });
            });


            /**
             * Bunch of events needed for infinite scroll feature and to keep sticky header visibility updated
             */
            var $win = $($window);
            scope.scrollable.on('scroll.collection-grid', function(e) {
                scope.loadNextIfSpaceAvailable();
                restoreStickyHeaderSizing();
                scope.updateFixedHeaderVisibility();
            });
            $win.on('resize.collection-grid', function(e) {
                scope.loadNextIfSpaceAvailable();
                restoreStickyHeaderSizing();
                scope.updateFixedHeaderVisibility();
            });
            scope.config.collection.bind('collectionLoadSuccess', function() {
                $timeout(function() {
                    scope.loadNextIfSpaceAvailable();
                    restoreStickyHeaderSizing();
                }, 50);
            });
            $(elm).find('.floating').hide();

            // Unbinding all the events
            scope.$on('$destroy', function(){
                scope.scrollable.off('scroll.collection-grid');
                $win.off('resize.collection-grid');
            });

            scope.$on('contextChanged', function() {
                scope.config.collection.load();
            });



            scope.combineFields = function() {
                var displayFields;
                if(Auth.myAccount.data.ui_property
                    && Auth.myAccount.data.ui_property.grid
                    && Auth.myAccount.data.ui_property.grid[scope.config.collection.objectName]
                    && Auth.myAccount.data.ui_property.grid[scope.config.collection.objectName].displayFields) {
                    displayFields = Auth.myAccount.data.ui_property.grid[scope.config.collection.objectName].displayFields;
                }
                if(displayFields || scope.config.displayFieldsDefault) {
                    scope.config.displayFields = _.map(displayFields || scope.config.displayFieldsDefault, function(name) {
                        return _.find(scope.config.fields, function(field) {
                            return field.name == name;
                        });
                    });
                } else {
                    scope.config.displayFields = scope.config.fields;
                }
                scope.config.hiddenFields = _.filter(scope.config.fields, function(field) {
                    return !_.find(scope.config.displayFields, function(df) {return df==field});
                });
            };
            scope.combineFields();
            // Watch field definitions to keep displayedFields in sync
            scope.$watch('config.fields', scope.combineFields);

            // -----------------------------------------------------------------

            /**
             * Opens edit column sequence dialog
             */
            scope.editColumns = function() {
                scope.columnConf = {
                    displayFields: angular.copy(scope.config.displayFields),
                    hiddenFields: angular.copy(scope.config.hiddenFields),
                    currentHidden: 0,
                    currentDisplayed: 0,
                    panel: 'hidden'
                };
                $(elm).find('.modal').unbind('keydown').keydown(function(event) {
                    if(event.keyCode == 9) {
                        // Tab button pressed
                        scope.columnConf.panel = scope.columnConf.panel=='hidden' ? 'displayed' : 'hidden';
                        scope.$apply();
                    } else if(event.keyCode == 38) {
                        // Up
                        if(scope.columnConf.panel=='hidden' && scope.columnConf.currentHidden>0) {
                            scope.columnConf.currentHidden--;
                            scope.$apply();
                        } else if(scope.columnConf.panel=='displayed' && scope.columnConf.currentDisplayed>0) {
                            scope.columnConf.currentDisplayed--;
                            scope.$apply();
                        }
                    } else if(event.keyCode == 40) {
                        // Down
                        if(scope.columnConf.panel=='hidden' && scope.columnConf.currentHidden < scope.config.hiddenFields.length ) {
                            scope.columnConf.currentHidden++;
                            scope.$apply();
                        } else if(scope.columnConf.panel=='displayed' && scope.columnConf.currentDisplayed < scope.config.displayFields.length) {
                            scope.columnConf.currentDisplayed++;
                            scope.$apply();
                        }
                    } else if(event.keyCode == 39) {
                        // Add column
                        scope.addColumn();
                        scope.$apply();
                    } else if(event.keyCode == 37) {
                        // Remove column
                        scope.removeColumn();
                        scope.$apply();
                    }
                });
                elm.find('.modal').aviModal();
            };

            /**
             * Adds the column at the position, if position not passed will add to the end
             * @param position - The position where new field will be injected
             */
            scope.addColumn = function(position) {
                if(scope.columnConf.panel!='hidden') {
                    return;
                }
                var item = scope.columnConf.hiddenFields[scope.columnConf.currentHidden];
                if(item) {
                    if(position!=undefined) {
                        scope.columnConf.displayFields.splice(position, 0, item);
                    } else {
                        scope.columnConf.displayFields.push(item);
                    }
                    scope.columnConf.hiddenFields = _.filter(scope.columnConf.hiddenFields, function(field) {
                        return field!=item;
                    });
                    if(scope.columnConf.hiddenFields.length && scope.columnConf.currentHidden >= scope.columnConf.hiddenFields.length) {
                        scope.columnConf.currentHidden = scope.columnConf.hiddenFields.length-1;
                    }
                }
            };

            /**
             * Removes column from the position
             * @param position
             */
            scope.removeColumn = function(position) {
                if(scope.columnConf.panel!='displayed') {
                    return;
                }
                var item = scope.columnConf.displayFields[scope.columnConf.currentDisplayed];
                if(item) {
                    if(position!=undefined) {
                        scope.columnConf.hiddenFields.splice(position, 0, item);
                    } else {
                        scope.columnConf.hiddenFields.push(item);
                    }
                    scope.columnConf.displayFields = _.filter(scope.columnConf.displayFields, function(field) {
                        return field!=item;
                    });
                    if(scope.columnConf.displayFields.length && scope.columnConf.currentDisplayed >= scope.columnConf.displayFields.length) {
                        scope.columnConf.currentDisplayed = scope.columnConf.displayFields.length-1;
                    }
                }
            };

            /**
             * Moves up currently selected column
             */
            scope.moveColumnUp = function() {
                var item, prevItem;
                if(scope.columnConf.panel=='displayed') {
                    item = scope.columnConf.displayFields[scope.columnConf.currentDisplayed];
                    prevItem = scope.columnConf.displayFields[scope.columnConf.currentDisplayed - 1];
                    if(item && prevItem) {
                        scope.columnConf.displayFields[scope.columnConf.currentDisplayed - 1] = item;
                        scope.columnConf.displayFields[scope.columnConf.currentDisplayed] = prevItem;
                        scope.columnConf.currentDisplayed--;
                    }
                } else if(scope.columnConf.panel=='hidden') {
                    item = scope.columnConf.hiddenFields[scope.columnConf.currentHidden];
                    prevItem = scope.columnConf.hiddenFields[scope.columnConf.currentHidden - 1];
                    if(item && prevItem) {
                        scope.columnConf.hiddenFields[scope.columnConf.currentHidden - 1] = item;
                        scope.columnConf.hiddenFields[scope.columnConf.currentHidden] = prevItem;
                        scope.columnConf.currentHidden--;
                    }
                }
            };

            /**
             * Moves down currently selected column
             */
            scope.moveColumnDown = function() {
                var item, nextItem;
                if(scope.columnConf.panel=='displayed') {
                    item = scope.columnConf.displayFields[scope.columnConf.currentDisplayed];
                    nextItem = scope.columnConf.displayFields[scope.columnConf.currentDisplayed + 1];
                    if(item && nextItem) {
                        scope.columnConf.displayFields[scope.columnConf.currentDisplayed + 1] = item;
                        scope.columnConf.displayFields[scope.columnConf.currentDisplayed] = nextItem;
                        scope.columnConf.currentDisplayed++;
                    }
                } else if(scope.columnConf.panel=='hidden') {
                    item = scope.columnConf.hiddenFields[scope.columnConf.currentHidden];
                    nextItem = scope.columnConf.hiddenFields[scope.columnConf.currentHidden + 1];
                    if(item && nextItem) {
                        scope.columnConf.hiddenFields[scope.columnConf.currentHidden + 1] = item;
                        scope.columnConf.hiddenFields[scope.columnConf.currentHidden] = nextItem;
                        scope.columnConf.currentHidden++;
                    }
                }
            };

            /**
             * Saves column sequence
             */
            scope.saveColumns = function() {
                if(!Auth.myAccount.data.ui_property) {
                    Auth.myAccount.data.ui_property = {};
                }
                if(!Auth.myAccount.data.ui_property.grid) {
                    Auth.myAccount.data.ui_property.grid = {};
                }
                if(!Auth.myAccount.data.ui_property.grid[scope.config.collection.objectName]) {
                    Auth.myAccount.data.ui_property.grid[scope.config.collection.objectName] = {};
                }
                Auth.myAccount.data.ui_property.grid[scope.config.collection.objectName].displayFields = _.map(scope.columnConf.displayFields, function(field) {
                    return field.name;
                });
                Auth.myAccount.saveDelayed();
                scope.combineFields();
                elm.find('.modal').aviModal('hide');

                // Fix field sizes, make sure field is not too narrow
                $timeout(function() {
                    restoreStickyHeaderSizing();
                    saveFieldsState();
                    // Restore percentage sizes for columns
                    var sizes = getColPercentSizes();
                    sizes.splice(0,1);
                    sizes.splice(-1,1);
                    var columns = $(elm).find('thead > tr:first-child > th');
                    columns.splice(0,1);
                    columns.splice(-1,1);
                    _.each(columns, function(col, index) {
                        if(sizes[index]<5) {
                            sizes[index] = 5;
                        }
                        $(col).css('width', sizes[index] + '%');
                    });
                });
            };

        }
    }
}]);

